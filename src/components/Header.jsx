import React from 'react';

const Header = () => (
  <h2>
    My Expense Tracker
  </h2>
);

export default Header;
